import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
   

public class CompareToTanggalan {
	public static void main(String args[]) throws ParseException{
		//  object tanggal
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		Date date1 = sdf.parse("2020-04-23");
		Date date2 = sdf.parse("2020-04-24");
		Date date3 = sdf.parse("2020-04-25");

		
		System.out.println("Date 1 : "+sdf.format(date1));
		System.out.println("Date 2 : "+sdf.format(date2));	
		System.out.println("Date 3 : "+sdf.format(date3));		

		// membandingkan dengan compareTo antara date 1 dengan date 2
		if(date1.compareTo(date2)==0) {
			System.out.println("date1 equals date2");
		}else {
			System.out.println("date1 not equals date2");
		}
		// membandingkan dengan compareTo antara date 2 dengan date 3
		if(date2.compareTo(date3)==0) {
			System.out.println("date2 equals date3");
		}else {
			System.out.println("date2 not equals date3");
		}
		// membandingkan dengan compareTo antara date 1 dengan date 3
		if(date1.compareTo(date3)==0) {
			System.out.println("date1 equals date3");
		}else {
			System.out.println("date1 not equals date3");
		}
		
	}	
}

